//

void main() {
//b)
  var easyEquities = new appDetails();

  easyEquities.nameOfTheApp = "EasyEquities";
  easyEquities.developer = "First World Trader";
  easyEquities.sector = "Finance";
  easyEquities.yearItWonMTNAward = 2020;

//print app details
  print(easyEquities.nameOfTheApp);
  print(easyEquities.developer);
  print(easyEquities.sector);
  print(easyEquities.yearItWonMTNAward);

  // print app name in Capital letters
  easyEquities.capitilize();
}

//a) a class
class appDetails {
  String? nameOfTheApp;
  String? sector;
  String? developer;
  int? yearItWonMTNAward;

  //b0) function that capitilize the app name
  void capitilize() {
    this.nameOfTheApp = nameOfTheApp?.toUpperCase();
    print("$nameOfTheApp");
  }
}
